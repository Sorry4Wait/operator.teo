<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Auto */
?>
<div class="auto-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
