<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Auto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mark_id')->textInput() ?>

    <?= $form->field($model, 'model_id')->textInput() ?>

    <?= $form->field($model, 'year_manufacture')->textInput() ?>

    <?= $form->field($model, 'state_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sts_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'resolution')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_sts_front_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_sts_reverse_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_license_front_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_license_reverse_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_a')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_v')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_g')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_salon')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
