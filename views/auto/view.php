<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Auto */
?>
<div class="auto-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mark_id',
            'model_id',
            'year_manufacture',
            'state_number',
            'sts_number',
            'resolution',
            'license',
            'foto_sts_front_side',
            'foto_sts_reverse_side',
            'foto_license_front_side',
            'foto_license_reverse_side',
            'foto_dot_a',
            'foto_dot_b',
            'foto_dot_v',
            'foto_dot_g',
            'foto_salon',
        ],
    ]) ?>

</div>
