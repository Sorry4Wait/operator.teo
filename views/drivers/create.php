<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Drivers */

?>
<div class="drivers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
