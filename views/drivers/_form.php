<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drivers-form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Выберите дату'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true, 
                'format' => 'yyyy-mm-dd',
            ]
        ]);
    ?>   

    <?= $form->field($model, 'password_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issued_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_date_passport')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Выберите дату'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true, 
                'format' => 'yyyy-mm-dd',
            ]
        ]);
    ?>   

    <?= $form->field($model, 'number_bu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'class_bu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_date_bu')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Выберите дату'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true, 
                'format' => 'yyyy-mm-dd',
            ]
        ]);
    ?>   

    <?= $form->field($model, 'callsign')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_foto_passport')->fileInput(); ?>

    <?= $form->field($model, 'file_foto_propiska')->fileInput(); ?>

    <?= $form->field($model, 'file_foto_bu_front_side')->fileInput(); ?>

    <?= $form->field($model, 'file_foto_bu_reverse_side')->fileInput(); ?>

    <?= $form->field($model, 'file_foto_driver')->fileInput(); ?>

    <?= $form->field($model, 'auto_id')->label()->widget(\kartik\select2\Select2::classname(), [
        'data' => $model->getAutos(),
        'options' => [
            'placeholder' => 'Выберите',
            ],
        'pluginOptions' => [ 
            'allowClear' => true
        ],
    ]); ?> 

    <?= $form->field($model, 'status_id')->label()->widget(\kartik\select2\Select2::classname(), [
        'data' => $model->getStatuses(),
        'options' => [
            'placeholder' => 'Выберите',
            ],
        'pluginOptions' => [ 
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'status_api')->label()->widget(\kartik\select2\Select2::classname(), [
        'data' => $model->getApiStatus(),
        'options' => [
            'placeholder' => 'Выберите',
            ],
        'pluginOptions' => [ 
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
