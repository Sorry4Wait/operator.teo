<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
?>
<div class="drivers-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
            'name',
            'middle_name',
            'phone',
            'birthday',
            'password_number',
            'issued_by',
            'issue_date_passport',
            'number_bu',
            'class_bu',
            'issue_date_bu',
            'callsign',
            'foto_passport',
            'foto_propiska',
            'foto_bu_front_side',
            'foto_bu_reverse_side',
            'foto_driver',
            'auto_id',
            'status_id',
            'creator_id',
            'activator_id',
            'date_create',
            'date_activation',
            'status_api',
            'comment:ntext',
        ],
    ]) ?>

</div>
