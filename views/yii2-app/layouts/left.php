<?php

use app\models\Users;

$permission = Yii::$app->user->identity->permission;
$id = Yii::$app->user->identity->id;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php 
        if(isset(Yii::$app->user->identity->id))
            { 
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'],],
                            ['label' => 'Таблица по сменам', 'icon' => 'file-text-o', 'url' => ['/change'],],
                            ['label' => 'Водители', 'icon' => 'file-text-o', 'url' => ['/drivers'],],
                            ['label' => 'Автомобили', 'icon' => 'file-text-o', 'url' => ['/auto'],],
                            [
                                'label' => 'Справочники',
                                'icon' => 'book',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Статус водителя', 'icon' => 'cube', 'url' => ['/driver-status'],],
                                    ['label' => 'Марка автомобиля', 'icon' => 'file-text-o', 'url' => ['/marks'],],
                                    ['label' => 'Модель автомобиля', 'icon' => 'file-text-o', 'url' => ['/models'],],
                                    ['label' => 'Настройки', 'icon' => 'file-text-o', 'url' => ['/settings'],],
                                ],
                            ],
                        ],
                    ]
                );
            }
        ?>

    </section>

</aside>
