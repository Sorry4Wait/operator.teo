<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Models */

?>
<div class="models-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
