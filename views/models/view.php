<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Models */
?>
<div class="models-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
