<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marks */

?>
<div class="marks-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
