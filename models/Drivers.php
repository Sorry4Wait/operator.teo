<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "drivers".
 *
 * @property int $id
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $middle_name Отчество
 * @property string $phone Телефон
 * @property string $birthday Дата рождение
 * @property string $password_number Номер паспорта
 * @property string $issued_by Кем выдан (паспорт)
 * @property string $issue_date_passport Дата выдачи паспорта
 * @property string $number_bu Номер ВУ
 * @property string $class_bu Класс ВУ
 * @property string $issue_date_bu Дата выдачи ВУ
 * @property string $callsign Позывной
 * @property string $foto_passport Фото Паспорт главная страница
 * @property string $foto_propiska Фото Паспорт прописка
 * @property string $foto_bu_front_side Фото ВУ лицевая страница
 * @property string $foto_bu_reverse_side Фото ВУ оборотная страница
 * @property string $foto_driver Фото водителя
 * @property int $auto_id Автомобиль
 * @property int $status_id Статус
 * @property int $creator_id Кто создал
 * @property int $activator_id Кто активировал
 * @property string $date_create Дата создания
 * @property string $date_activation Дата активации
 * @property int $status_api Статус Отправки по апи
 * @property string $comment Комментарий 
 *
 * @property Users $activator
 * @property Auto $auto
 * @property Users $creator
 * @property DriverStatus $status
 */
class Drivers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file_foto_passport;
    public $file_foto_propiska;
    public $file_foto_bu_front_side;
    public $file_foto_bu_reverse_side;
    public $file_foto_driver;

    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday', 'issue_date_passport', 'issue_date_bu', 'date_create', 'date_activation'], 'safe'],
            [['auto_id', 'status_id', 'creator_id', 'activator_id', 'status_api'], 'integer'],
            [['comment'], 'string'],
            [['file_foto_passport', 'file_foto_propiska', 'file_foto_driver', 'file_foto_bu_reverse_side', 'file_foto_bu_front_side'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['surname', 'name', 'middle_name', 'phone', 'password_number', 'issued_by', 'number_bu', 'class_bu', 'callsign', 'foto_passport', 'foto_propiska', 'foto_bu_front_side', 'foto_bu_reverse_side', 'foto_driver'], 'string', 'max' => 255],
            [['activator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['activator_id' => 'id']],
            [['auto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auto::className(), 'targetAttribute' => ['auto_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => DriverStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'middle_name' => 'Отчество',
            'phone' => 'Телефон',
            'birthday' => 'Дата рождение',
            'password_number' => 'Номер паспорта',
            'issued_by' => 'Кем выдан (паспорт)',
            'issue_date_passport' => 'Дата выдачи паспорта',
            'number_bu' => 'Номер ВУ',
            'class_bu' => 'Класс ВУ',
            'issue_date_bu' => 'Дата выдачи ВУ',
            'callsign' => 'Позывной',
            'foto_passport' => 'Фото Паспорт главная страница',
            'foto_propiska' => 'Фото Паспорт прописка',
            'foto_bu_front_side' => 'Фото ВУ лицевая страница',
            'foto_bu_reverse_side' => 'Фото ВУ оборотная страница',
            'foto_driver' => 'Фото водителя',
            'auto_id' => 'Автомобиль',
            'status_id' => 'Статус',
            'creator_id' => 'Кто создал',
            'activator_id' => 'Кто активировал',
            'date_create' => 'Дата создания',
            'date_activation' => 'Дата активации',
            'status_api' => 'Статус Отправки по апи',
            'comment' => 'Комментарий ',

            'file_foto_passport' => 'Фото Паспорт главная страница',
            'file_foto_propiska' => 'Фото Паспорт прописка',
            'file_foto_bu_front_side' => 'Фото ВУ лицевая страница',
            'file_foto_bu_reverse_side' => 'Фото ВУ оборотная страница',
            'file_foto_driver' => 'Фото водителя',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->creator_id = Yii::$app->user->identity->id;
            $this->date_create = date('Y-m-d');
        }

        /*$this->activator_id = Yii::$app->user->identity->id;
        $this->date_activation = date('Y-m-d');*/

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivator()
    {
        return $this->hasOne(Users::className(), ['id' => 'activator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuto()
    {
        return $this->hasOne(Auto::className(), ['id' => 'auto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DriverStatus::className(), ['id' => 'status_id']);
    }

    public function getAutos()
    {
        $auto = Auto::find()->all();
        return ArrayHelper::map($auto,'id', 'state_number');
    }

    public function getStatuses()
    {
        $auto = DriverStatus::find()->all();
        return ArrayHelper::map($auto,'id', 'name');
    }

    public function getApiStatus()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Сделано',],
            ['id' => '2','type' => 'Ошибка',],
            ['id' => '3','type' => 'Не сделанно',],  
        ], 'id', 'type');
    }
}
