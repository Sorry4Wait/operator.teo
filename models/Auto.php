<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto".
 *
 * @property int $id
 * @property int $mark_id Марка автомобиля
 * @property int $model_id Модель автомобиля
 * @property int $year_manufacture Год выпуска авто
 * @property string $state_number Гос. номер
 * @property string $sts_number Номер СТС
 * @property string $resolution Разрешение
 * @property string $license Серия и № лицензии
 * @property string $foto_sts_front_side Фото СТС лицевая сторона
 * @property string $foto_sts_reverse_side Фото СТС оборотная сторона
 * @property string $foto_license_front_side Фото Лицензии лицевая сторона
 * @property string $foto_license_reverse_side Фото Лицензии лицевая сторона
 * @property string $foto_dot_a Фото автомобиля с точка А
 * @property string $foto_dot_b Фото автомобиля с точка Б
 * @property string $foto_dot_v Фото автомобиля с точка В
 * @property string $foto_dot_g Фото автомобиля с точка Г
 * @property string $foto_salon Фото салона
 *
 * @property Marks $mark
 * @property Models $model
 * @property Drivers[] $drivers
 */
class Auto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark_id', 'model_id', 'year_manufacture'], 'integer'],
            [['state_number', 'sts_number', 'resolution', 'license', 'foto_sts_front_side', 'foto_sts_reverse_side', 'foto_license_front_side', 'foto_license_reverse_side', 'foto_dot_a', 'foto_dot_b', 'foto_dot_v', 'foto_dot_g', 'foto_salon'], 'string', 'max' => 255],
            [['mark_id'], 'exist', 'skipOnError' => true, 'targetClass' => Marks::className(), 'targetAttribute' => ['mark_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Models::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark_id' => 'Марка автомобиля',
            'model_id' => 'Модель автомобиля',
            'year_manufacture' => 'Год выпуска авто',
            'state_number' => 'Гос. номер',
            'sts_number' => 'Номер СТС',
            'resolution' => 'Разрешение',
            'license' => 'Серия и № лицензии',
            'foto_sts_front_side' => 'Фото СТС лицевая сторона',
            'foto_sts_reverse_side' => 'Фото СТС оборотная сторона',
            'foto_license_front_side' => 'Фото Лицензии лицевая сторона',
            'foto_license_reverse_side' => 'Фото Лицензии лицевая сторона',
            'foto_dot_a' => 'Фото автомобиля с точка А',
            'foto_dot_b' => 'Фото автомобиля с точка Б',
            'foto_dot_v' => 'Фото автомобиля с точка В',
            'foto_dot_g' => 'Фото автомобиля с точка Г',
            'foto_salon' => 'Фото салона',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMark()
    {
        return $this->hasOne(Marks::className(), ['id' => 'mark_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Models::className(), ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Drivers::className(), ['auto_id' => 'id']);
    }
}
