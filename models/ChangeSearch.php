<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Change;

/**
 * ChangeSearch represents the model behind the search form about `app\models\Change`.
 */
class ChangeSearch extends Change
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'minut', 'activation', 'average_speed', 'who_paid'], 'integer'],
            [['begin_datetime', 'end_datetime', 'status'], 'safe'],
            [['pay_sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Change::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'begin_datetime' => $this->begin_datetime,
            'end_datetime' => $this->end_datetime,
            'minut' => $this->minut,
            'activation' => $this->activation,
            'average_speed' => $this->average_speed,
            'pay_sum' => $this->pay_sum,
            'who_paid' => $this->who_paid,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
