<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auto;

/**
 * AutoSearch represents the model behind the search form about `app\models\Auto`.
 */
class AutoSearch extends Auto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mark_id', 'model_id', 'year_manufacture'], 'integer'],
            [['state_number', 'sts_number', 'resolution', 'license', 'foto_sts_front_side', 'foto_sts_reverse_side', 'foto_license_front_side', 'foto_license_reverse_side', 'foto_dot_a', 'foto_dot_b', 'foto_dot_v', 'foto_dot_g', 'foto_salon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mark_id' => $this->mark_id,
            'model_id' => $this->model_id,
            'year_manufacture' => $this->year_manufacture,
        ]);

        $query->andFilterWhere(['like', 'state_number', $this->state_number])
            ->andFilterWhere(['like', 'sts_number', $this->sts_number])
            ->andFilterWhere(['like', 'resolution', $this->resolution])
            ->andFilterWhere(['like', 'license', $this->license])
            ->andFilterWhere(['like', 'foto_sts_front_side', $this->foto_sts_front_side])
            ->andFilterWhere(['like', 'foto_sts_reverse_side', $this->foto_sts_reverse_side])
            ->andFilterWhere(['like', 'foto_license_front_side', $this->foto_license_front_side])
            ->andFilterWhere(['like', 'foto_license_reverse_side', $this->foto_license_reverse_side])
            ->andFilterWhere(['like', 'foto_dot_a', $this->foto_dot_a])
            ->andFilterWhere(['like', 'foto_dot_b', $this->foto_dot_b])
            ->andFilterWhere(['like', 'foto_dot_v', $this->foto_dot_v])
            ->andFilterWhere(['like', 'foto_dot_g', $this->foto_dot_g])
            ->andFilterWhere(['like', 'foto_salon', $this->foto_salon]);

        return $dataProvider;
    }
}
