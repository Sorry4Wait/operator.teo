<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "change".
 *
 * @property int $id
 * @property string $begin_datetime Дата и время начала
 * @property string $end_datetime Дата и время завершения 
 * @property int $minut Кол-во минут
 * @property int $activation Активаций 
 * @property int $average_speed Средняя скорость
 * @property double $pay_sum Сумма оплаты
 * @property string $status Статус
 * @property int $who_paid Кто оплатил
 *
 * @property Users $whoPa
 */
class Change extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'change';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin_datetime', 'end_datetime'], 'safe'],
            [['minut', 'activation', 'average_speed', 'who_paid'], 'integer'],
            [['pay_sum'], 'number'],
            [['status'], 'string', 'max' => 20],
            [['who_paid'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['who_paid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'begin_datetime' => 'Дата и время начала',
            'end_datetime' => 'Дата и время завершения ',
            'minut' => 'Кол-во минут',
            'activation' => 'Активаций ',
            'average_speed' => 'Средняя скорость',
            'pay_sum' => 'Сумма оплаты',
            'status' => 'Статус',
            'who_paid' => 'Кто оплатил',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhoPa()
    {
        return $this->hasOne(Users::className(), ['id' => 'who_paid']);
    }
}
