<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $permission Должность
 * @property double $balance Баланс
 * @property int $party_system_id Ид сторонней системы
 *
 * @property Change[] $changes
 */
class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE_ADMIN = 'administrator';
    const USER_ROLE_AGENT = 'agent';
    const USER_ROLE_ACTIVATOR = 'activator';

    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balance'], 'number'],
            [['party_system_id'], 'integer'],
            [['fio', 'login', 'password', 'permission', 'new_password'], 'string', 'max' => 255],
            [['login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'permission' => 'Должность',
            'balance' => 'Баланс',
            'party_system_id' => 'Ид сторонней системы',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->password = md5($this->password);
        }

        if($this->new_password != null) 
        {
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_AGENT, 'name' => 'Агент',],
            ['id' => self::USER_ROLE_ACTIVATOR, 'name' => 'Активатор',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {        
        if(self::USER_ROLE_ADMIN == $this->permission) return 'Администратор';
        if(self::USER_ROLE_AGENT == $this->permission) return 'Агент';
        if(self::USER_ROLE_ACTIVATOR == $this->permission) return 'Активатор';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanges()
    {
        return $this->hasMany(Change::className(), ['who_paid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Drivers::className(), ['activator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers0()
    {
        return $this->hasMany(Drivers::className(), ['creator_id' => 'id']);
    }

}
