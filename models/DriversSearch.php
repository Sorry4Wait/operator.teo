<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Drivers;

/**
 * DriversSearch represents the model behind the search form about `app\models\Drivers`.
 */
class DriversSearch extends Drivers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'auto_id', 'status_id', 'creator_id', 'activator_id', 'status_api'], 'integer'],
            [['surname', 'name', 'middle_name', 'phone', 'birthday', 'password_number', 'issued_by', 'issue_date_passport', 'number_bu', 'class_bu', 'issue_date_bu', 'callsign', 'foto_passport', 'foto_propiska', 'foto_bu_front_side', 'foto_bu_reverse_side', 'foto_driver', 'date_create', 'date_activation', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Drivers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'issue_date_passport' => $this->issue_date_passport,
            'issue_date_bu' => $this->issue_date_bu,
            'auto_id' => $this->auto_id,
            'status_id' => $this->status_id,
            'creator_id' => $this->creator_id,
            'activator_id' => $this->activator_id,
            'date_create' => $this->date_create,
            'date_activation' => $this->date_activation,
            'status_api' => $this->status_api,
        ]);

        $query->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'password_number', $this->password_number])
            ->andFilterWhere(['like', 'issued_by', $this->issued_by])
            ->andFilterWhere(['like', 'number_bu', $this->number_bu])
            ->andFilterWhere(['like', 'class_bu', $this->class_bu])
            ->andFilterWhere(['like', 'callsign', $this->callsign])
            ->andFilterWhere(['like', 'foto_passport', $this->foto_passport])
            ->andFilterWhere(['like', 'foto_propiska', $this->foto_propiska])
            ->andFilterWhere(['like', 'foto_bu_front_side', $this->foto_bu_front_side])
            ->andFilterWhere(['like', 'foto_bu_reverse_side', $this->foto_bu_reverse_side])
            ->andFilterWhere(['like', 'foto_driver', $this->foto_driver])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
