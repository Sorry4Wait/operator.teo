<?php

namespace app\controllers;

use Yii;
use app\models\Drivers;
use app\models\DriversSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * DriversController implements the CRUD actions for Drivers model.
 */
class DriversController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Drivers models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new DriversSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Drivers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Водитель",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Drivers();  
        if ($model->load($request->post()) && $model->save()) 
        {

            $model->file_foto_passport = UploadedFile::getInstance($model, 'file_foto_passport');
            if(!empty($model->file_foto_passport))
            {
                $model->file_foto_passport->saveAs('uploads/'.$model->file_foto_passport->baseName.'.'.$model->file_foto_passport->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_passport' => $model->file_foto_passport->baseName.'.'.$model->file_foto_passport->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_propiska = UploadedFile::getInstance($model, 'file_foto_propiska');
            if(!empty($model->file_foto_propiska))
            {
                $model->file_foto_propiska->saveAs('uploads/'.$model->file_foto_propiska->baseName.'.'.$model->file_foto_propiska->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_propiska' => $model->file_foto_propiska->baseName.'.'.$model->file_foto_propiska->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_bu_front_side = UploadedFile::getInstance($model, 'file_foto_bu_front_side');
            if(!empty($model->file_foto_bu_front_side))
            {
                $model->file_foto_bu_front_side->saveAs('uploads/'.$model->file_foto_bu_front_side->baseName.'.'.$model->file_foto_bu_front_side->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_bu_front_side' => $model->file_foto_bu_front_side->baseName.'.'.$model->file_foto_bu_front_side->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_bu_reverse_side = UploadedFile::getInstance($model, 'file_foto_bu_reverse_side');
            if(!empty($model->file_foto_bu_reverse_side))
            {
                $model->file_foto_bu_reverse_side->saveAs('uploads/'.$model->file_foto_bu_reverse_side->baseName.'.'.$model->file_foto_bu_reverse_side->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_bu_reverse_side' => $model->file_foto_bu_reverse_side->baseName.'.'.$model->file_foto_bu_reverse_side->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_driver = UploadedFile::getInstance($model, 'file_foto_driver');
            if(!empty($model->file_foto_driver))
            {
                $model->file_foto_driver->saveAs('uploads/'.$model->file_foto_driver->baseName.'.'.$model->file_foto_driver->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_driver' => $model->file_foto_driver->baseName.'.'.$model->file_foto_driver->extension], [ 'id' => $model->id ])->execute();
            }


            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }       
    }

    /**
     * Updates an existing Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if ($model->load($request->post()) && $model->save()) {

            $model->file_foto_passport = UploadedFile::getInstance($model, 'file_foto_passport');
            if(!empty($model->file_foto_passport))
            {
                $model->file_foto_passport->saveAs('uploads/'.$model->file_foto_passport->baseName.'.'.$model->file_foto_passport->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_passport' => $model->file_foto_passport->baseName.'.'.$model->file_foto_passport->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_propiska = UploadedFile::getInstance($model, 'file_foto_propiska');
            if(!empty($model->file_foto_propiska))
            {
                $model->file_foto_propiska->saveAs('uploads/'.$model->file_foto_propiska->baseName.'.'.$model->file_foto_propiska->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_propiska' => $model->file_foto_propiska->baseName.'.'.$model->file_foto_propiska->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_bu_front_side = UploadedFile::getInstance($model, 'file_foto_bu_front_side');
            if(!empty($model->file_foto_bu_front_side))
            {
                $model->file_foto_bu_front_side->saveAs('uploads/'.$model->file_foto_bu_front_side->baseName.'.'.$model->file_foto_bu_front_side->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_bu_front_side' => $model->file_foto_bu_front_side->baseName.'.'.$model->file_foto_bu_front_side->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_bu_reverse_side = UploadedFile::getInstance($model, 'file_foto_bu_reverse_side');
            if(!empty($model->file_foto_bu_reverse_side))
            {
                $model->file_foto_bu_reverse_side->saveAs('uploads/'.$model->file_foto_bu_reverse_side->baseName.'.'.$model->file_foto_bu_reverse_side->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_bu_reverse_side' => $model->file_foto_bu_reverse_side->baseName.'.'.$model->file_foto_bu_reverse_side->extension], [ 'id' => $model->id ])->execute();
            }

            $model->file_foto_driver = UploadedFile::getInstance($model, 'file_foto_driver');
            if(!empty($model->file_foto_driver))
            {
                $model->file_foto_driver->saveAs('uploads/'.$model->file_foto_driver->baseName.'.'.$model->file_foto_driver->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_driver' => $model->file_foto_driver->baseName.'.'.$model->file_foto_driver->extension], [ 'id' => $model->id ])->execute();
            }
            
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete an existing Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Drivers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Drivers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Drivers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
