<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto`.
 */
class m180928_094521_create_auto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auto', [
            'id' => $this->primaryKey(),
            'mark_id' => $this->integer()->comment('Марка автомобиля'),
            'model_id' => $this->integer()->comment('Модель автомобиля'),
            'year_manufacture' => $this->integer()->comment('Год выпуска авто'),
            'state_number' => $this->string(255)->comment('Гос. номер'),
            'sts_number' => $this->string(255)->comment('Номер СТС'),
            'resolution' => $this->string(255)->comment('Разрешение'),
            'license' => $this->string(255)->comment('Серия и № лицензии'),
            'foto_sts_front_side' => $this->string(255)->comment('Фото СТС лицевая сторона'),
            'foto_sts_reverse_side' => $this->string(255)->comment('Фото СТС оборотная сторона'),
            'foto_license_front_side' => $this->string(255)->comment('Фото Лицензии лицевая сторона'),
            'foto_license_reverse_side' => $this->string(255)->comment('Фото Лицензии лицевая сторона'),
            'foto_dot_a' => $this->string(255)->comment('Фото автомобиля с точка А'),
            'foto_dot_b' => $this->string(255)->comment('Фото автомобиля с точка Б'),
            'foto_dot_v' => $this->string(255)->comment('Фото автомобиля с точка В'),
            'foto_dot_g' => $this->string(255)->comment('Фото автомобиля с точка Г'),
            'foto_salon' => $this->string(255)->comment('Фото салона'),
        ]);

        $this->addCommentOnTable('settings', 'Автомобили');

        $this->createIndex('idx-auto-mark_id', 'auto', 'mark_id', false);
        $this->addForeignKey("fk-auto-mark_id", "auto", "mark_id", "marks", "id");

        $this->createIndex('idx-auto-model_id', 'auto', 'model_id', false);
        $this->addForeignKey("fk-auto-model_id", "auto", "model_id", "models", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-auto-mark_id','auto');
        $this->dropIndex('idx-auto-mark_id','auto');

        $this->dropForeignKey('fk-auto-model_id','auto');
        $this->dropIndex('idx-auto-model_id','auto');

        $this->dropTable('auto');
    }
}
