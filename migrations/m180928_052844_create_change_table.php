<?php

use yii\db\Migration;

/**
 * Handles the creation of table `change`.
 */
class m180928_052844_create_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('change', [
            'id' => $this->primaryKey(),
            'begin_datetime' => $this->datetime()->comment('Дата и время начала'),
            'end_datetime' => $this->datetime()->comment('Дата и время завершения '),
            'minut' => $this->integer()->comment('Кол-во минут'),
            'activation' => $this->integer()->comment('Активаций '),
            'average_speed' => $this->integer()->comment('Средняя скорость'),
            'pay_sum' => $this->float()->comment('Сумма оплаты'),
            'status' => $this->string(20)->comment('Статус'),  //payed-Оплачено; no_payed-Не оплачено;
            'who_paid' => $this->integer()->comment('Кто оплатил'),
        ]);

        $this->addCommentOnTable('settings', 'Таблица по сменам');

        $this->createIndex('idx-change-who_paid', 'change', 'who_paid', false);
        $this->addForeignKey("fk-change-who_paid", "change", "who_paid", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-change-who_paid','change');
        $this->dropIndex('idx-change-who_paid','change');

        $this->dropTable('change');
    }
}
