<?php

use yii\db\Migration;

/**
 * Handles the creation of table `models`.
 */
class m180928_052042_create_models_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('models', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);

        $this->addCommentOnTable('marks', 'Модель автомобиля');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('models');
    }
}
