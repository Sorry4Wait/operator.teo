<?php

use yii\db\Migration;

/**
 * Handles the creation of table `driver_status`.
 */
class m180928_051019_create_driver_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('driver_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'color' => $this->string(10)->comment('Цвет'),
        ]);

        $this->addCommentOnTable('driver_status', 'Статус водителя');

        $this->insert('driver_status',array(
            'name' => 'Свободен',
            'color' => '#3c8dbc',
        ));

        $this->insert('driver_status',array(
            'name' => 'На Активации',
            'color' => '#3cbc7a',
        ));

        $this->insert('driver_status',array(
            'name' => 'Отклонен',
            'color' => '#e81044',
        ));

        $this->insert('driver_status',array(
            'name' => 'Активирован',
            'color' => '#16e810',
        ));

        $this->insert('driver_status',array(
            'name' => 'Не подтвержденный',
            'color' => '#e81010',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('driver_status');
    }
}
