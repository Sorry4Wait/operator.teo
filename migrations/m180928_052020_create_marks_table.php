<?php

use yii\db\Migration;

/**
 * Handles the creation of table `marks`.
 */
class m180928_052020_create_marks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('marks', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);

        $this->addCommentOnTable('marks', 'Марка автомобиля');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('marks');
    }
}
